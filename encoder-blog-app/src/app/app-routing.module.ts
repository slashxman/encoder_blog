import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; 
import { HomeComponent } from './components/common/home/home.component';
import { ContactComponent } from './components/common/contact/contact.component';
import { BlogComponent } from './components/common/blog/blog.component';
import { PortfolioComponent } from './components/common/portfolio/portfolio.component';

const routes: Routes = [
  {
    path: "",
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path:"portfolio",
    component: PortfolioComponent
  },
  {
    path: "blog",
    component: BlogComponent
  },
  {
    path: "contact",
    component: ContactComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
