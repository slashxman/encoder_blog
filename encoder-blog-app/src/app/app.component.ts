import { Component } from '@angular/core';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';


// src/app/app.component.ts

@Component({
  selector: 'app-root',
  template: `
  <!-- header -->
  <app-header></app-header>

  <!-- routes will be rendered here -->
  <router-outlet></router-outlet>

  <!-- footer -->
  <app-footer></app-footer>
  `,
  styles: []
})
export class AppComponent {
  faCoffee = faCoffee;
}
