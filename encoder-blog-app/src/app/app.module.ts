import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/common/header/header.component';
import { FooterComponent } from './components/common/footer/footer.component';
import { HomeComponent } from './components/common/home/home.component';
import { ContactComponent } from './components/common/contact/contact.component';
import { SendContactFormComponent } from './components/common/send-contact-form/send-contact-form.component';
import { BlogComponent } from './components/common/blog/blog.component';
import { PortfolioComponent } from './components/common/portfolio/portfolio.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ContactComponent,
    SendContactFormComponent,
    BlogComponent,
    PortfolioComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    AppRoutingModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
