import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendContactFormComponent } from './send-contact-form.component';

describe('SendContactFormComponent', () => {
  let component: SendContactFormComponent;
  let fixture: ComponentFixture<SendContactFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendContactFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendContactFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
