import { Component, OnInit } from '@angular/core';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faSquare, faCheckSquare } from '@fortawesome/free-solid-svg-icons';
import { faTwitter, faTelegram, faBitbucket, faLinkedin } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.css']
})
export class HeaderComponent implements OnInit {

  isBurgerMenuActive: boolean = false;

  constructor(library: FaIconLibrary) {
    library.addIcons(faSquare, faCheckSquare, faTwitter, faBitbucket, faTelegram, faLinkedin);
  }

  ngOnInit() {
  }

  burgerMenuClick() {
    this.isBurgerMenuActive = !this.isBurgerMenuActive;
  }

}
